/*
 * Class: 	HugeInteger
 * 
 * Version: 9.26.2017.2052
 *
 * Date:	26 September, 2017
 * 
 * This work has no copyright.
 */

/* 
 * In places where one instance of HugeInteger is referencing another
 *  instance I used the this operator to improve readability.
 *  
 * WARNING: If the number to be stored in digits is ever greater than
 * 	NUM_DIGITS long future operations on digits will produce invalid results.
 */
public class HugeInteger{
	public static final int NUM_DIGITS=40;
	private int digits[]=new int[NUM_DIGITS];
	private boolean positive; // polarity of the number stored in digits[]
	private int HugeIntegerLength; // length of number stored in digits[]

	/**
	* Converts an integer string to an array of digits.
	*
	* It does not check the validity of the string.
	*
	* @param num integer string
	*/
	public HugeInteger(String number){
		positive = (number.contains("-"))?false:true; 
		if (!positive) { 
			number = number.substring(1); // remove "-" sign
		}
		
		HugeIntegerLength = number.length();
		
		// align number so LSD is at the end of digits[]
		int j = (HugeIntegerLength <= NUM_DIGITS)?
					NUM_DIGITS-HugeIntegerLength:0; 
		
		for (int i : parseIntArray(number)){  
			if (j < NUM_DIGITS) {
				digits[j++] = i;
			}
		}
	}
	
	/**
	* Converts an integer string to an array of digits.
	*
	* It does not check the validity of the string.
	*
	* @param number integer string
	*/
	public static int [] parseIntArray(String number){
		return java.util.regex.Pattern.compile("").splitAsStream(number)
								.mapToInt(Integer::parseInt).toArray();
	}
	
	/**
	* Checks if the HugeInteger.digits array contains only zeros.
	*
	* It does not check the validity of the string.
	*
	* @return boolean true if all zeros
	*/
	public boolean isZero() {
		boolean isZero = true;
		for (int digit : digits) {
			if (digit != 0) {
				isZero = false;
				break;
			}
		}
		return isZero;
	}
	
	/**
	 * Is a private method and not for outside users to call. This method
	 * interprets array1 and array2 as two integers whose LSB digits are
	 * at array1[NUM_DIGITS-1] and array2[NUM_DIGITS-1]. Without sign
 	 * information, array1 and array2 can only represent positive integers.
	 *
	 * This method adds array1 and array2, and store the result in another
	 * integer array for return.
	 *
	 * @param array1 first integer
	 * @param array2 second integer
	 * @return addition of array1 and array2
	 *
	 */
	private static int [] addArrayDigits(int [] array1, int [] array2){
		int result[] = new int[NUM_DIGITS];
		for (int i=NUM_DIGITS-1; i > -1; i--) {
			result[i] += array1[i] + array2[i];
			
			// handle carry digits
			if (result[i] > 9) {
				if (i > 0) { 	// skip if at result[0]
					result[i-1] += result[i]/10; 
				}
				result[i] %= 10; // remove the carry from the sum
			}
		}
		return result;
	}
	
	/**
	* Is a private method and not for outside users to call. This method
	* interprets array1 and array2 as two integers whose LSB digits are
	* at array1[NUM_DIGITS-1] and array2[NUM_DIGITS-1]. Without sign
	* information, array1 and array2 can only represent positive integers.
	*
	* This method subtracts array2 from array1, and store the result in
	* another integer array for return.
	*
	* The assumption is the integer in array1 is greater than the one in
	* array2
	*
	* @param array1 first integer
	* @param array2 second integer
	* @return subtraction of array2 from array1
	*/
	private static int [] subtractArrayDigits(int [] array1, int [] array2){
		int result[] = new int[NUM_DIGITS];
		for (int i=NUM_DIGITS-1; i > -1; i--) {
			
			// Check if we need to borrow from the next higher number.
			if (i > 0 && array1[i] < array2[i]) { 
				result[i-1] -= 1; 	// carry a -1 to the next digit
				result[i] += 10; 	// add the borrow to this digit
			}
			result[i] += array1[i] - array2[i];
			
			// If the resultant digit is -1, perform a borrow operation.
			if (i > 0 && result[i] < 0) {
				result[i] += 10;
				result[i-1] -= 1; // carry a -1 to the next digit
			}
		}
		return result;
	}
	
	/**
	 * Is a private method and not for outside users to call. This method
	 * interprets array1 and array2 as two integers whose LSB digits are
	 * at array1[NUM_DIGITS-1] and array2[NUM_DIGITS-1]. Without sign
 	 * information, array1 and array2 can only represent positive integers.
	 *
	 * This method multiplies array1 and array2, and store the result in another
	 * integer array for return.
	 *
	 * @param array1 first integer
	 * @param array2 second integer
	 * @return product of array1 and array2
	 *
	 */
	private static int [] multiplyArrayDigits(int [] array1, int [] array2){
		// The last element is used to avoid ArrayIndexOutOfBoundsException
		int[] result = new int[NUM_DIGITS + 1];
		
		for (int i = NUM_DIGITS - 1; i > -1; i--) {
			// r is used for the result index
			for (int j = NUM_DIGITS - 1, r = i; r > -1; j--, r--) {
				// Add the current results element value to the product of
				//	the specified digits elements then add the carry value.
				result[r] += array1[i] * array2[j] + result[r+1]/10;
				
				// replace the current element with the remainder value
				result[r+1] %= 10;
			}
			
			// If the number is larger than NUM_DIGITS in length, the carry is
			//	discarded and the remainder replaces the value in result[0].
			result[0] %= 10;
		}
		
		// remove the extra last element
		return java.util.Arrays.copyOf(result, NUM_DIGITS);
	}
	
	/**
	 * Is a private method and not for outside users to call. This method
	 * interprets array1 and array2 as two integers whose LSB digits are
	 * at array1[NUM_DIGITS-1] and array2[NUM_DIGITS-1]. Without sign
 	 * information, array1 and array2 can only represent positive integers.
	 *
	 * This method compares if the value in array1 is larger than the value in
	 * array2, and stores the result in a boolean variable for return.
	 * 
	 * @param array1 first integer
	 * @param array2 second integer
	 * @return boolean true if array1 is greater
	 */
	private static boolean isGreaterArraysDigits(int [] array1, int [] array2){
		boolean leftGreater = false;
		for (int i=0; i < NUM_DIGITS; i++) {
			if(array1[i] == array2[i]) {
				continue;
			}
			leftGreater = (array1[i] > array2[i])?true:false;
			break;
		}
		return leftGreater;
	}
	
	// Converts digits[] into a string.
	@Override
	public String toString(){
		String hiString = (positive)? "": "-";
		
		if (isZero()) {
			hiString = "0";
		} else {
			boolean leadingZero = true;
			for (int digit : digits) {
				leadingZero = (leadingZero && digit==0)?true:false;
				hiString += (!leadingZero)? digit: "";
			}
		}
		return hiString;
	}
	
	// Updates the length of the number in digits and returns that length.
	public int length(){
		String number = toString();
		HugeIntegerLength = (number.contains("-"))? 
								number.length()-1: number.length();
		return HugeIntegerLength;
	}

	// Toggles the positive boolean value.
	public void negate(){ 
		positive = (positive == true)?false:true; 
	}

	// Is this HugeInteger equal to another HugeInteger?
	public boolean isEqualTo(HugeInteger hi){
		return this.positive == hi.positive 
				&& java.util.Arrays.equals(this.digits, hi.digits);
	}
	
	public boolean isNotEqualTo(HugeInteger hi){
		return !isEqualTo(hi);
	}
	
	/* For readability I opted to remove short-circuit logic tests. */
	// Tests for true conditions and assumes false otherwise.
	public boolean isGreaterThan(HugeInteger hi){
		boolean isGreater = false;
		if (this.positive && !hi.positive) { // positive > negative
			isGreater = true;
		} else if (this.positive == hi.positive){ 
				boolean thisLarger = isGreaterArraysDigits(this.digits,
														hi.digits);
				
				// This is positive and larger or negative and closer to zero.
				if (this.positive && thisLarger 
						|| !this.positive && !thisLarger){
					isGreater = true;
			}
		}
		return isGreater;
	}
	
	public boolean isLessThan(HugeInteger hi){
		return !isGreaterThanOrEqualTo(hi);
	}
	
	public boolean isGreaterThanOrEqualTo(HugeInteger hi){
		return isGreaterThan(hi) || isEqualTo(hi);
	}
	
	public boolean isLessThanOrEqualTo(HugeInteger hi){
		return isLessThan(hi) || isEqualTo(hi);
	}
	
	// Add another HugeInteger number to this HugeInteger number.
	public void add(HugeInteger hi){
		if(this.positive != hi.positive){  // it is in fact doing subtraction
			
			// The result is zero
			if(isEqualTo(hi)) {  
				this.positive = true;
				java.util.Arrays.fill(this.digits, 0);
				
			} else if(this.positive){  // this is positive, hi is negative
				// Negate hi temporarily so we have two positive numbers.
				hi.negate(); 
				
				if(this.isGreaterThan(hi)){ // |this| > |hi| [e.g. 8 + (-5)]
					this.digits = HugeInteger.subtractArrayDigits(this.digits,
																 hi.digits);
				} else {					// |this| <= |hi| [e.g. 8 + (-15)]
					this.digits = HugeInteger.subtractArrayDigits(hi.digits,
																 this.digits); 
					this.negate();	// The result is actually negative.
				}
				hi.negate();  // restore hi's polarity
				
			}else{  // this is negative, hi is positive
				this.negate(); // make this positive
				if(this.isGreaterThan(hi)) { 	// [e.g. (-8) > 5]
					this.digits = subtractArrayDigits(this.digits, hi.digits);
				} else { 						// [e.g. (-8) < 15]
					this.digits = subtractArrayDigits(hi.digits, this.digits);
					this.negate(); // toggle to negative
				}
				this.negate(); // restore this' polarity
			}
		}else{
			// same sign, easy :)
			this.digits = addArrayDigits(this.digits, hi.digits);
		}	
	}
	
	// Subtract another HugeInteger number from this HugeInteger number.
	public void subtract(HugeInteger hi){	
		// Subtracting is the same as adding a negative number. 
		// [e.g. 5 - (-8) = 5 + 8 or 5 - 8 = 5 + (-8)]
		hi.negate();  // Temporarily toggle hi's polarity
		
		// add already performs addition and subtraction functions.
		add(hi);
		
		hi.negate();  // Restore hi's polarity.
	}

	// Multiplies this.digits with another HugeInteger.digits array
	//	and replaces this.digits with the product.
	public void multiply(HugeInteger hi){		
		this.digits = multiplyArrayDigits(this.digits, hi.digits);
		
		// only negative if opposite polarity
		this.positive = (this.positive != hi.positive)?false:true; 
	}
}